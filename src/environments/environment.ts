// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: "http://localhost/angular/exampleTest2/slim/",
  firebase:{
    apiKey: "AIzaSyDGY4r5KMr_BKW0FphUpgPpHtemxLDbTxU",
    authDomain: "users-2ff0c.firebaseapp.com",
    databaseURL: "https://users-2ff0c.firebaseio.com",
    projectId: "users-2ff0c",
    storageBucket: "users-2ff0c.appspot.com",
    messagingSenderId: "624539424617"
  }
};
