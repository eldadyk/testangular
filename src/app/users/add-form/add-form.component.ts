
//Q3
import { UsersService } from './../users.service';
import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { FormGroup , FormControl ,FormBuilder, Validators} from '@angular/forms';
import { Router } from "@angular/router"; //Login without JWT


@Component({
  selector: 'add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {
  @Output() addUser:EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;

  addform = new FormGroup({
    name:new FormControl(),
    phonenumber:new FormControl()
  });

  sendData() {
    this.addUser.emit(this.addform.value.name);
    
    console.log(this.addform.value);
    this.service.postUsers(this.addform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  }
  //Login without JWT router
  constructor(service: UsersService,  private formBuilder:FormBuilder, private router:Router) {
    this.service = service;
   }

  ngOnInit() {
    this.addform = this.formBuilder.group({
      name:  [null, [Validators.required]],
      phonenumber: [null, [Validators.required]],
    });
    //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
     // this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }  
  }

}
